


```console
docker run --rm -it --user 1000:1000 -v $PWD/test-data:/test-data docker.io/terradue/stars:1.0.0-beta.11 \
    Stars copy \
    -r 4 \
    -rel \
    -o /test-data/staged \
    https://earth-search.aws.element84.com/v0/collections/sentinel-s2-l2a-cogs/items/S2B_53HPA_20210723_0_L2A
```

Stage test data with:

```console
docker run --rm -it --user 1000:1000 -v $PWD/test-data:/test-data docker.io/terradue/stars:1.0.0-beta.9 \
    Stars copy \
    -r 4 \
    -rel \
    -o /test-data/staged \
    file:///test-data/catalog.json
```