calrissian  --stdout \
          /calrissian-output/results.json \
          --stderr \
          /calrissian-output/app.log \
          --max-ram \
          16G \
          --max-cores \
          "8" \
          --tmp-outdir-prefix \
          /calrissian-tmp/ \
          --outdir \
          /calrissian-output/ \
          --usage-report \
          /calrissian-output/usage.json \
          stage.cwl \
           --input_reference https://earth-search.aws.element84.com/v0/collections/sentinel-s2-l2a-cogs/items/S2B_53HPA_20210723_0_L2A 