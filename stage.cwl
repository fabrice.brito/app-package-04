$graph:
- class: CommandLineTool
  id: main
  requirements:
    DockerRequirement: 
      dockerPull: docker.io/terradue/stars:1.0.0-beta.26
    InlineJavascriptRequirement: {}
    EnvVarRequirement:
        envDef:
          PATH: /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
          AWS__ServiceURL: https://s3.pl-waw.scw.cloud
          #AWS__ServiceURL: scw
          AWS__Region: pl-waw
          AWS_ACCESS_KEY_ID: SCW97Q14M0BWZQB92Z29
          AWS_SECRET_ACCESS_KEY: 94336c5f-cd32-469c-9735-aeca8fcd749e
          "Credentials__StacCatalog__AuthType" : "Basic"
          "Credentials__StacCatalog__UriPrefix" : "https://supervisor.charter.uat.esaportal.eu/"
          "Credentials__StacCatalog__Username" : "stars"
          "Credentials__StacCatalog__Password" : "89126578214hr87fh8g2389"
          # STARS_URL_FIND: "https?://supervisor.charter.uat.esaportal.eu/catalog"
          # STARS_URL_REPLACE: "http://acceptance-supervisor-catalog-stac.cpe/catalog"

  baseCommand: 
  - Stars
  - copy
  arguments:
  - -r 
  - "4"
  - -rel 
  - --harvest
  - -o 
  - ./
  - $( inputs.input_reference ) 
  inputs: 
    input_reference: 
      type: string
  outputs:
    staged:
      outputBinding:
        glob: .
      type: Directory
cwlVersion: v1.0